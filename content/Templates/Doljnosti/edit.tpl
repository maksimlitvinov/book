{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <div class="ui segment">
            <a href="/doljnosti/" class="ui green button">К списку должностей</a>
        </div>
        <div class="ui segment">
            {include 'Doljnosti/_form.tpl'}
        </div>
    </div>
{/block}