{extends 'Base.tpl'}

{block 'sidebar'}{/block}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <table class="ui striped celled compact table">
            <thead>
                <tr>
                    <th>More</th>
                    <th>Название</th>
                    <th>Телефон</th>
                    <th>E-mail</th>
                    <th>Фактический адрес</th>
                    <th>ИНН</th>
                    <th>КПП</th>
                    <th>Банк</th>
                    <th>Директор</th>
                    <th>Основание</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                {foreach $firms as $firm}
                    <tr>
                        <td>
                            <a href="/firms/more/{$firm.id}">
                                <i class="newspaper icon"></i>
                            </a>
                        </td>
                        <td>{$firm.name}</td>
                        <td>{$firm.fone}</td>
                        <td>{$firm.email}</td>
                        <td>{$firm.fadress}</td>
                        <td>{$firm.inn}</td>
                        <td>{$firm.kpp}</td>
                        <td>{$firm.bank}</td>
                        <td>{$firm.fdir} {$firm.idir} {$firm.odir}</td>
                        <td>{$firm.osnov}</td>
                        <td></td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{/block}