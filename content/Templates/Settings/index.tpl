{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>Настройки</h1>

        <div class="ui segment">
            <div class="ui top attached tabular menu">
                <a href="#" class="active item" data-tab="base">Общие</a>
                <a href="#" class="item" data-tab="users">Пользователи</a>
            </div>
            <div class="ui bottom attached active tab segment" data-tab="base">Общие </div>
            <div class="ui bottom attached tab segment" data-tab="users">
                <a href="/settings/users/create/" class="ui button green">Добавить пользователя</a>
                <table class="ui table celled">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Логин</th>
                            <th>Ф.И.О.</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Должность</th>
                            <th>Партнер</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $settings.users as $user}
                        <tr>
                            <td>{$user.id}</td>
                            <td>{$user.login}</td>
                            <td>{$user.last_name} {$user.user_name} {$user.otchestvo}</td>
                            <td>{$user.phone}</td>
                            <td>{$user.email}</td>
                            <td>{$user.doljnost.doljnost}</td>
                            <td>{$user.partner_id}</td>
                            <td>
                                <div class="ui grid column">
                                    <a href="/settings/users/edit/{$user.id}" title="Редактировать" class="column">
                                        <i class="edit icon"></i>
                                    </a>
                                    <a href="/settings/users/delete/{$user.id}" class="column" title="Удалить">
                                        <i class="ban icon"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>

    </div>
{/block}