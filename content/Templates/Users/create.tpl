{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        {include 'Users/_form.tpl'}
    </div>
{/block}