<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{$pagetitle}</title>
        <link rel="stylesheet" type="text/css" href="{$assets_url}libs/semantic/dist/semantic.min.css">
    </head>
    <body>
        {block 'header'}
            <div class="ui fluid container"> {* Base block *}
                <div class="ui attached stackable menu">
                    <a class="item" href="/">Home</a>
                    <a class="item right" href="/settings/">Настройки</a>
                    <a class="item right" href="/user/">User</a>
                    <a class="item right" href="/logout/">Выход</a>
                </div>
                <div class="ui grid"> {* Base Grid *}
        {/block}

        {block 'sidebar'}
                    <div class="three wide column">
                        <p>SideBar</p>
                    </div>
        {/block}

        {block 'template'}

        {/block}

        {block 'footer'}
                </div> {* End Base Grid *}
            </div> {* End Base block *}
        {/block}

        <script src="{$assets_url}libs/jquery/dist/jquery.min.js"></script>
        <script src="{$assets_url}libs/semantic/dist/semantic.min.js"></script>
        <script src="{$assets_url}js/custom.js"></script>

    </body>
</html>