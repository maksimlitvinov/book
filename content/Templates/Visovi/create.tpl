{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>


        <form action="/visovi/add/" method="post" class="ui form">
            <div class="ui segment">
                <h4 class="ui dividing header">Информация о вызове</h4>
                <div class="field">
                    <label>Выберите партнера</label>
                    <select class="ui fluid dropdown" name="partner">
                        <option value="0">Выберите партнера</option>
                    </select>
                </div>
                <div class="field">
                    <label>Дата и время</label>
                    <div class="two fields">
                        <div class="field">
                            <input type="text" name="dateforengineer">
                        </div>
                        <div class="field">
                            <input type="text" name="timeforengineer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui segment">
                <h4 class="ui dividing header">Информация о клиенте</h4>
                <div class="field">
                    <label>Телефон для связи</label>
                    <input type="text" name="fone">
                </div>
                <div class="field">
                    <select class="ui fluid dropdown" name="who">
                        <option value="0">Физическое лицо</option>
                        <option value="1">Юридическое лицо</option>
                    </select>
                </div>
                <div class="field urik">
                    <select name="urik" class="ui fluid search dropdown">
                        <option selected disabled value="0">Выберите организацию</option>
                    </select>
                </div>

                <div class="fisik">
                    <div class="field">
                        <div class="ui toggle checkbox">
                            <input type="checkbox" name="invk" tabindex="0">
                            <label>Состоит в группе VK?</label>
                        </div>
                    </div>
                    <div class="field vk">
                        <label>Выберите участника</label>
                        <div class="ui fluid search selection dropdown">
                            <input type="hidden" name="receipt">
                            <i class="dropdown icon"></i>
                            <div class="default text">Сохранить контакты</div>
                            <div class="menu">
                                <div class="item" data-value="jenny" data-text="Дженни"><img class="ui mini avatar image" src="/images/avatar/small/jenny.jpg"> Jenny Hess </div>
                                <div class="item" data-value="elliot" data-text="Элиот"><img class="ui mini avatar image" src="/images/avatar/small/elliot.jpg"> Elliot Fu </div>
                                <div class="item" data-value="stevie" data-text="Stevie"><img class="ui mini avatar image" src="/images/avatar/small/stevie.jpg"> Stevie Feliciano </div>
                                <div class="item" data-value="christian" data-text="Christian"><img class="ui mini avatar image" src="/images/avatar/small/christian.jpg"> Christian </div>
                                <div class="item" data-value="matt" data-text="Matt"><img class="ui mini avatar image" src="/images/avatar/small/matt.jpg"> Matt </div>
                                <div class="item" data-value="justen" data-text="Джастин"><img class="ui mini avatar image" src="/images/avatar/small/justen.jpg"> Justen Kitsune </div>
                            </div>
                        </div>
                    </div>
                    <div class="field notvk">
                        <label for="">Имя заказавшего вызов</label>
                        <input type="text" name="namek">
                    </div>
                    <div class="fields">
                        <div class="seven wide field">
                            <label>Улица</label>
                            <input type="text" name="street">
                        </div>
                        <div class="three wide field">
                            <label>Дом</label>
                            <input type="text" name="home">
                        </div>
                        <div class="six wide field">
                            <label>Корпус</label>
                            <input type="text" name="housing">
                        </div>
                        <div class="six wide field">
                            <label>Квартира / Офис</label>
                            <input type="text" name="apartment">
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
{/block}