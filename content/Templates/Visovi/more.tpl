{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <a href="/visovi/" title="К списку вызовов" class="circular ui icon button">
            <i class="icon arrow left"></i>
        </a>
        <table class="ui striped table">
            <tbody>
                <tr>
                    <td>ID вызова</td>
                    <td>{$visov.id}</td>
                </tr>
                <tr>
                    <td>Дата вызова</td>
                    <td>{$visov.dateforengineer | date : 'd-m-Y'}</td>
                </tr>
                <tr>
                    <td>Время вызова</td>
                    <td>{$visov.timeforengineer}</td>
                </tr>
                <tr>
                    <td>Адрес</td>
                    {if $visov.adress}
                        <td>{$visov.adres}</td>
                    {else}
                        <td>{$visov.street}, {$visov.home}, {$visov.housing}, {$visov.apartment}</td>
                    {/if}
                </tr>
                <tr>
                    <td>Проблема заявленная клиентом</td>
                    <td>{$visov.problemsk}</td>
                </tr>
                <tr>
                    <td>Инженер</td>
                    <td>{$visov.engineer}</td>
                </tr>
                <tr>
                    <td>Клиент</td>
                    <td>{$visov.namek}</td>
                </tr>
                <tr>
                    <td>Организация</td>
                    <td>{$visov.firm.name}</td>
                </tr>
                <tr>
                    <td>Партнер</td>
                    <td>{$visov.partner}</td>
                </tr>
                <tr>
                    <td>Статус</td>
                    <td>{$visov.status.name}</td>
                </tr>
                <tr>
                    <td>Состоит ли в группе vk</td>
                    <td>{$visov.vkgroup}</td>
                </tr>
                <tr>
                    <td>Номер квитанции</td>
                    <td>{$visov.kvitancia}</td>
                </tr>
                <tr>
                    <td>Проблема указанная инженером</td>
                    <td>{$visov.problemse}</td>
                </tr>
                <tr>
                    <td>Скидка</td>
                    <td>{$visov.sale}</td>
                </tr>
                <tr>
                    <td>К орлате</td>
                    <td>{$visov.koplate}</td>
                </tr>
            </tbody>
        </table>
    </div>
{/block}