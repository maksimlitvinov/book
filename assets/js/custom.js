jQuery(function($) {
    $('.ui.dropdown').dropdown('refresh');

    /** Uploads avatar on profile **/
    var fileExtentionRange = '.jpg .jpeg .png .JPG .gif .bmp';
    var MAX_SIZE = 3; // MB

    $(document).on('change', '.btn-file :file', function(e) {
        var input = $(this);

        if (navigator.appVersion.indexOf("MSIE") != -1) { // IE
            var label = input.val();

            input.trigger('fileselect', [ 1, label, 0 ]);
        } else {
            var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            var numFiles = input.get(0).files ? input.get(0).files.length : 1;
            var size = input.get(0).files[0].size;
            var file = input.get(0).files;

            input.trigger('fileselect', [ numFiles, label, size, file ]);
        }
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label, size, file) {
        $('#attachmentName').attr('name', 'attachmentName'); // allow upload.

        var postfix = label.substr(label.lastIndexOf('.'));
        if (fileExtentionRange.indexOf(postfix.toLowerCase()) > -1) {
            if (size > 1024 * 1024 * MAX_SIZE ) {
                alert('max size：<strong>' + MAX_SIZE + '</strong> MB.');

                $('#attachmentName').removeAttr('name'); // cancel upload file.
            } else {
                $('#_attachmentName').val(label);
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function (e) {
                        $('.avatar-tmp img').attr({
                            src: e.target.result,
                            alt: theFile.name
                        });
                    }
                    /*return function (e) {
                        $("<span>", {
                            append: $("<img>", {
                                src: e.target.result,
                                alt: theFile.name,
                            }),
                        }).appendTo('.avatar-tmp');
                    }*/
                })(file);
                reader.readAsDataURL(file[0]);
            }
        } else {
            alert('file type：<br/> <strong>' + fileExtentionRange + '</strong>');

            $('#attachmentName').removeAttr('name'); // cancel upload file.
        }
    });
    /** End Uploads avatar on profile **/

    /** Табы в настройках **/
    $('.menu .item').tab();

    /** Чекбокс партнера в форме пользователя **/
    var $partner = $('input[type="checkbox"][name="partner_id"]');
    if($partner.prop('checked')) {
        $('select[name="doljnost_id"]').val(5);
        $('select[name="doljnost_id"] option').each(function () {
            if($(this).val() != 5) {
                $(this).prop('disabled', true);
            }else {
                $(this).prop('disabled', false);
            }
        });
    }else{
        $('select[name="doljnost_id"] option').each(function () {
            if($(this).val() == 5  || $(this).val() == 0) {
                $(this).prop('disabled', true);
            }else{
                $(this).prop('disabled', false);
            }
        });
    }
    $partner.on('change', function () {
        if($(this).prop('checked')) {
            $('select[name="doljnost_id"]').val(5);
            $('select[name="doljnost_id"] option').each(function () {
                if($(this).val() != 5) {
                    $(this).prop('disabled', true);
                }else {
                    $(this).prop('disabled', false);
                }
            });
        }else {
            $('select[name="doljnost_id"] option').each(function () {
                if($(this).val() == 5  || $(this).val() == 0) {
                    $(this).prop('disabled', true);
                }else{
                    $(this).prop('disabled', false);
                }
            });
            $('select[name="doljnost_id"]').val(0);
        }

    })

});