<?php

namespace Book\Modules\Auth;

use Book\Helper\Cookie;
use Book\Helper\Session;

class Auth implements AuthInterface
{

    /**
     * @return null
     */
    public static function authorized()
    {
        return Session::get('auth_authorized');
    }

    /**
     * @return mixed
     */
    public static function hasUser()
    {
        //return Cookie::get('auth_user');
        return Session::get('auth_user');
    }

    /**
     * @param $user
     */
    public static function authorize($user)
    {
        /*Cookie::set('auth_authorized', true);
        Cookie::set('auth_user', $user);*/

        Session::set('auth_authorized', true);
        Session::set('auth_user', $user);
    }

    /**
     *
     */
    public static function unAuthorize()
    {
        /*Cookie::delete('auth_authorized');
        Cookie::delete('auth_user');*/

        Session::delete('auth_authorized');
        Session::delete('auth_user');
    }

    /**
     * @return string
     */
    public static function salt() {
        return (string) rand(10000000, 99999999);
    }

    /**
     * @param $password
     * @param string $salt
     * @return string
     */
    public static function encryptPassword($password, $salt = '') {
        return hash('sha256', $password . $salt);
    }
}