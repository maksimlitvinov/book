<?php

namespace Book\Modules\Thumb;

use phpthumb as phpThumb;
use Book\Helper\Log;

class Thumb extends phpThumb
{

    private $allowed_ext = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];

    /**
     * Thumb constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $cachePath = $this->getCacheDir();
        $this->setParameter('config_cache_directory', $cachePath);
        $this->setParameter('config_temp_directory', $cachePath);
        $this->setCacheDirectory();

        $this->setParameter('config_allow_src_above_docroot',false);
        $this->setParameter('config_cache_maxage',30 * 86400);
        $this->setParameter('config_cache_maxsize',100 * 1024 * 1024);
        $this->setParameter('config_cache_maxfiles',10000);
        $this->setParameter('config_error_bgcolor','CCCCFF');
        $this->setParameter('config_error_textcolor','FF0000');
        $this->setParameter('config_error_fontsize',1);
        $this->setParameter('config_nohotlink_enabled',true);
        $this->setParameter('config_nohotlink_valid_domains',PROJECT_SITE_URL);
        $this->setParameter('config_nohotlink_erase_image',true);
        $this->setParameter('config_nohotlink_text_message','Off-server thumbnailing is not allowed');
        $this->setParameter('config_nooffsitelink_enabled',false);
        $this->setParameter('config_nooffsitelink_valid_domains',PROJECT_SITE_URL);
        $this->setParameter('config_nooffsitelink_require_refer',false);
        $this->setParameter('config_nooffsitelink_erase_image',true);
        $this->setParameter('config_nooffsitelink_watermark_src','');
        $this->setParameter('config_nooffsitelink_text_message','Off-server linking is not allowed');
        $this->setParameter('cache_source_enabled',false);
        $this->setParameter('cache_source_directory',$cachePath.'source/');
        $this->setParameter('allow_local_http_src',true);
        $this->setParameter('zc',0);
        $this->setParameter('far','C');
        $this->setParameter('cache_directory_depth',4);
        $this->setParameter('config_ttf_directory', $this->getFontsDir());

        $this->setParameter('config_output_maxwidth', 1920);
        $this->setParameter('config_output_maxheight', 1080);
        $this->setParameter('config_output_format', 'jpeg');
        $this->setParameter('q', 90);

        $this->setParameter('config_document_root',PROJECT_BASE_PATH);

        $this->setParameter('config_disable_debug', true);
    }

    /**
     * @return string
     */
    private function getCacheDir()
    {
        if (!file_exists(PROJECT_CACHE_THUMB_PATH)) {
            mkdir(PROJECT_CACHE_THUMB_PATH, 0755, true);
        }

        return PROJECT_CACHE_THUMB_PATH;
    }

    /**
     * @return string
     */
    private function getFontsDir()
    {
        if (!file_exists(PROJECT_ASSETS_PATH . 'fonts/thumb/')) {
            mkdir(PROJECT_ASSETS_PATH . 'fonts/thumb/', 0755, true);
        }

        return PROJECT_ASSETS_PATH . 'fonts/thumb/';
    }

    public function optimized($file)
    {

        $res = explode('.', $file);
        $count = count($res);
        $ext = strtolower($res[$count -1]);
        if (!in_array($ext, $this->allowed_ext)) {
            return false;
        } else {
            $this->setParameter('config_output_format', $ext);
        }

        $this->setSourceFilename($file);
        $this->GenerateThumbnail();

        if ($this->RenderToFile($file)) {
            return $response = ['error' => '0', 'messages' => 'Изображение загружено успешно.'];
        } else {
            unlink($file);
            Log::log('Thumb: Не могу обработать изображение.');
            return $response = ['error' => '1', 'messages' => 'Не могу обработать изображение.'];
        }
    }
}