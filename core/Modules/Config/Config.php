<?php

namespace Book\Modules\Config;

use Book\DI\DI;

class Config
{
    /**
     * @var DI
     */
    public $di;

    /**
     * Config constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
    }

    /**
     * @param $group
     * @return mixed
     */
    public function getConfig($group)
    {
        $req = $this->di->get('req');
        return require $req->server['DOCUMENT_ROOT'] . '/core/Config/' . $group . 'Config.php';
    }

    /**
     * @return mixed
     */
    public function getMainConfig()
    {
        $req = $this->di->get('req');
        return require $req->server['DOCUMENT_ROOT'] . '/core/Config/MainConfig.php';
    }
}