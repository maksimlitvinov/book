<?php
/**
 * List Routes
 * @var $this->router = new \Book\Modules\Router\Router()
 */

// Auth
$this->router->add('home', '/', 'LoginController:index');
$this->router->add('login', '/login/', 'LoginController:login', 'POST');
$this->router->add('logout', '/logout/', 'LoginController:logout');

// Base razdels
$this->router->add('visovi', '/visovi/', 'VisovController:index');
$this->router->add('visov-edit', '/visovi/edit/(id:int)', 'VisovController:edit');
$this->router->add('visov-delete', '/visovi/delete/(id:int)', 'VisovController:delete');
$this->router->add('visov-more', '/visovi/more/(id:int)', 'VisovController:more');
$this->router->add('visov-create', '/visovi/create/', 'VisovController:create');
$this->router->add('visov-add', '/visovi/add/', 'VisovController:add', 'POST');
$this->router->add('visov-update', '/visovi/update/(id:int)', 'VisovController:update', 'POST');

$this->router->add('avr', '/avr/', 'AvrController:index');

$this->router->add('settings', '/settings/', 'SettingsController:index');
$this->router->add('settings-user-create', '/settings/users/create/', 'SettingsController:createUser');
$this->router->add('settings-user-edit', '/settings/users/edit/(id:int)', 'SettingsController:editUser');
$this->router->add('settings-user-delete', '/settings/users/delete/(id:int)', 'SettingsController:deleteUser');
$this->router->add('settings-user-add', '/settings/users/add/', 'SettingsController:addUser', 'POST');
$this->router->add('settings-user-update', '/settings/users/update/', 'SettingsController:updateUser', 'POST');

// Должности
$this->router->add('doljnosti', '/doljnosti/', 'DoljnostController:index');
$this->router->add('doljnosti-create', '/doljnosti/create/', 'DoljnostController:create');
$this->router->add('doljnosti-edit', '/doljnosti/edit/(id:int)', 'DoljnostController:edit');
$this->router->add('doljnosti-delete', '/doljnosti/delete/(id:int)', 'DoljnostController:delete');
$this->router->add('doljnosti-update', '/doljnosti/update/', 'DoljnostController:update', 'POST');
$this->router->add('doljnosti-add', '/doljnosti/add/', 'DoljnostController:add', 'POST');
$this->router->add('doljnosti-restore', '/doljnosti/restore/(id:int)', 'DoljnostController:restore');

$this->router->add('sklad', '/sklad/', 'SkladController:index');

$this->router->add('statistic', '/stat/', 'StatisticController:index');

$this->router->add('workshop', '/workshop/', 'WorkShopController:index');

$this->router->add('firms', '/firms/', 'FirmController:index');
$this->router->add('firms-more', '/firms/more/(id:int)', 'FirmController:more');

$this->router->add('user', '/user/', 'UserController:index');
$this->router->add('user-edit', '/user/edit/', 'UserController:edit');
$this->router->add('user-update', '/user/update/', 'UserController:update', 'POST');

// Error
$this->router->add('404', '/404.html', 'Page404Controller:index');