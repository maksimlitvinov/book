<?php

namespace Book\Modules\Router;

class Router
{
    private $di;

    private $routes = [];

    private $host;

    private $dispatcher;

    /**
     * Router constructor.
     */
    public function __construct($di)
    {
        $this->di = $di;
        $this->host = 'http://journal.ru/';
    }

    /**
     * @param $key
     * @param $pattern
     * @param $controller
     * @param string $method
     */
    public function add($key, $pattern, $controller, $method = 'GET')
    {
        $this->routes[$key] = [
            'pattern' => $pattern,
            'controller' => $controller,
            'method' => $method
        ];
    }

    /**
     * @param $method
     * @param $uri
     * @return DispatchedRoute
     */
    public function dispatch($method, $uri) {
        return $this->getDispatcher()->dispatch($method, $uri);
    }

    /**
     * @return UrlDispatcher
     */
    public function getDispatcher() {
        if($this->dispatcher == null) {
            $this->dispatcher = new UrlDispatcher();

            foreach($this->routes as $route) {
                $this->dispatcher->register($route['method'], $route['pattern'], $route['controller']);
            }
        }

        return $this->dispatcher;
    }

    public function errorRedirect($key, $code)
    {
        $ctrl = $this->routes[$key]['controller'];
        list($controller, $action) = explode(':', $ctrl);
        $controller = '\Book\Controllers\Error\\' . $controller;
        $cn = new $controller($this->di);
        $cn->{$action}();
        exit;
    }
}