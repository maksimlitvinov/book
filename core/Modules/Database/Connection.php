<?php

namespace Book\Modules\Database;

use Book\DI\DI;
use Illuminate\Database\Capsule\Manager as Capsule;

class Connection
{
    /**
     * @var DI
     */
    private $di;

    public $capsule;

    private $db_config;

    public function __construct(DI $di)
    {
        $this->di = $di;
        $cfg_di = $this->di->get('config');
        $this->db_config = $cfg_di->getConfig('DB');

        return $this->connect();
    }

    public function connect()
    {
        $this->capsule = new Capsule();
        $this->capsule->addConnection($this->db_config);
        $this->capsule->bootEloquent();
        return $this->capsule;
    }
}