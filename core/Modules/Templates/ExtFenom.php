<?php

namespace Book\Modules\Templates;

use Book\Models\Doljnost;

class ExtFenom
{
    public $getDoljnost = 'getDoljnost';

    public static function getDoljnost($id = null)
    {
        if (empty($id)) {
            return Doljnost::all();
        }

        $doljnost = Doljnost::where('id', $id)->get();
        return $doljnost[0];
    }
}