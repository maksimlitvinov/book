<?php

namespace Book\Controllers;

use Book\Models\Visov;

class VisovController extends BaseController
{

    public $pagetitle = 'Книга вызовов | Журнал СЦ ПочиникА';

    public $data = [];

    public function index()
    {
        $this->data = Visov::with('partner', 'status', 'firm')->orderBy('status_id', 'asc')->groupBy('id')->get()->toArray();

        echo $this->template->render('Visovi/index', $this->getDataToTemplate('visovi'));
    }

    public function create()
    {
        $this->pagetitle = 'Добавление вызова | Журнал СЦ ПочиникА';
        echo $this->template->render('Visovi/create', $this->getDataToTemplate());
    }

    public function edit($id)
    {
        var_dump($id);
    }

    public function delete($id)
    {
        var_dump($id);
    }

    public function more($id)
    {
        $this->pagetitle = 'Информация о вызове | Журнал СЦ ПочиникА';

        $res = Visov::with('partner', 'status', 'firm')->where('id', $id)->get();
        $this->data = $res[0];

        echo $this->template->render('Visovi/more', $this->getDataToTemplate('visov'));
    }

    public function add()
    {

    }

    public function update($id)
    {

    }
}