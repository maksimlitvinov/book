<?php

namespace Book\Controllers\Error;

use Book\Controllers\CommonTrait;
use Book\DI\DI;

class Page404Controller
{

    use CommonTrait;

    /**
     * @var DI
     */
    protected $di;
    /**
     * @var \Book\Modules\Templates\Template
     */
    protected $template;
    /**
     * @var string
     */
    public $pagetitle = 'Страница не найдена | Журнал СЦ ПочиникА';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var array
     */
    public $deleted = [];
    /**
     * @var string
     */
    public $action = '';
    /**
     * @var array
     */
    public $currentUser = [];

    /**
     * Page404Controller constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->template = $this->di->get('template');
    }

    /**
     * Base Method
     */
    public function index()
    {
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");

        echo $this->template->render('Error/notFound', $this->getDataToTemplate());
    }
}