<?php

namespace Book\Controllers;

use Book\Models\Visov;
use Book\Models\WorkShop;

class StatisticController extends BaseController
{

    public $pagetitle = 'Статистика';

    public function index()
    {
        $this->data['visovi'] = Visov::with('partner', 'status', 'firm')
                                        ->where('engineer', $this->currentUser->id)
                                        ->where('status_id', 1)
                                        ->orderBy('status_id', 'asc')
                                        ->groupBy('id')
                                        ->get()->toArray();

        $this->data['workshop'] = [];

        echo $this->template->render('Statistic/index', $this->getDataToTemplate());
    }
}