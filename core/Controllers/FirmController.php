<?php

namespace Book\Controllers;

use Book\Models\Firm;

class FirmController extends BaseController
{

    public $pagetitle = 'Список клиентов | Журнал СЦ ПочиникА';

    public $data = [];

    public function index()
    {

        $this->data = Firm::all()->toArray();
        echo $this->template->render('Firms/index', $this->getDataToTemplate('firms'));
    }

    public function more($id)
    {
        $res = Firm::where('id', $id)->get();
        $this->data = $res[0];
        echo $this->template->render('Firms/more', $this->getDataToTemplate('firm'));
    }

    public function create()
    {

    }

    public function edit($id)
    {

    }

    public function delete($id)
    {

    }

    public function add()
    {

    }

    public function update()
    {

    }
}