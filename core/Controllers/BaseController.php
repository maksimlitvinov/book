<?php

namespace Book\Controllers;

use Book\DI\DI;
use Book\Modules\Auth\Auth;
use Book\Controllers\UserController as User;
use Book\Helper\Log;
use Book\Helper\Session;

abstract class BaseController
{

    use CommonTrait;

    /**
     * @var DI
     */
    public $di;
    /**
     * @var \Book\Modules\Http\Request
     */
    public $req;
    /**
     * @var \Book\Modules\Config\Config
     */
    public $config;
    /**
     * @var \Book\Modules\Database\Connection
     */
    public $db;
    /**
     * @var \Book\Modules\Router\Router
     */
    public $router;
    /**
     * @var \Book\Modules\Templates\Template
     */
    public $template;
    /**
     * @var \Book\Modules\Thumb\Thumb
     */
    public $thumb;
    /**
     * @var \Book\Modules\Files\Files
     */
    public $files;

    /**
     * @var string
     */
    public $pagetitle = 'Журнал СЦ ПочиникА';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var string
     */
    public $action = '';
    /**
     * @var array
     */
    public $deleted = [];
    /**
     * @var array
     */
    public $currentUser = [];

    public function __construct(DI $di)
    {
        $this->di = $di;

        $this->req      = $this->di->get('req');
        $this->config   = $this->di->get('config');
        $this->db       = $this->di->get('db');
        $this->router   = $this->di->get('router');
        $this->template = $this->di->get('template');
        $this->thumb    = $this->di->get('thumb');
        $this->files    = $this->di->get('files');

        if (!Auth::authorized()) {
            self::Redirect();
        }

        $this->currentUser = User::getUserForLogin(Auth::hasUser());
    }

    public static function Redirect($url = '/')
    {
        header("Location: {$url}");
        exit();
    }
}