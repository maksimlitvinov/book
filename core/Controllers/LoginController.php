<?php

namespace Book\Controllers;

use Book\DI\DI;
use Book\Controllers\UserController as User;
use Book\Helper\Log;
use Book\Helper\Session;
use Book\Modules\Auth\Auth;

class LoginController
{

    use CommonTrait;

    /**
     * @var DI
     */
    public $di;
    /**
     * @var \Book\Modules\Http\Request
     */
    public $req;
    /**
     * @var \Book\Modules\Auth\Auth
     */
    public $auth;
    /**
     * @var \Book\Modules\Templates\Template
     */
    public $template;
    /**
     * @var string
     */
    public $pagetitle = 'Журнал СЦ ПочиникА';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var
     */
    public $action;
    /**
     * @var
     */
    public $deleted;
    /**
     * @var array
     */
    public $currentUser = [];

    /**
     * LoginController constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->template = $this->di->get('template');
        $this->req = $this->di->get('req');
        $this->auth = $this->di->get('auth');
    }

    /**
     * Base Method
     */
    public function index()
    {
        if (Auth::hasUser()) {
            self::Redirect('/stat/');
        }

        echo $this->template->render('Login/index', $this->getDataToTemplate());
    }

    /**
     * Login Method. Only POST
     */
    public function login()
    {
        if (!$fromForm = $this->req->getPost()) {
            self::Redirect();
        }

        $user = User::getUserForLogin($fromForm['login']);

        if (empty($user)) {
            $this->errorLogin('CheckLogin', 'Неправильное имя пользователя или пароль.', 'Неверный логин: ' . $fromForm['login']);
        }

        if (self::checkErrorLogin($user)) {
            $this->errorLogin('CheckErrorLogin', 'Пользователь заблокирован. Обратитесь к администратору.', 'Пользователь заблокирован: ' . $fromForm['login']);
        }

        $pass = Auth::encryptPassword($fromForm['pass'], $user->salt);
        if ($pass != $user->pass) {
            $user->login_error = (int) $user->login_error + 1;
            User::uib(['id' => $user->id, 'login_error' => $user->login_error], '', false);
            $this->errorLogin('CheckPassword', 'Неправильное имя пользователя или пароль.', 'Неверный пароль: ' . $fromForm['pass']);
        }

        Auth::authorize($fromForm['login']);

        self::Redirect('/stat/');
    }

    /**
     * Logout Method
     */
    public function logout()
    {
        Auth::unAuthorize();
        self::Redirect();
    }

    /**
     * @param $step
     * @param string $message
     * @param string $privatMes
     */
    public function errorLogin($step, $message = '', $privatMes = '')
    {
        Log::log($step . ': ' . $privatMes, 'login');
        Session::set('messages', $message);
        self::Redirect();
    }

    /**
     * @param string $url
     */
    public static function Redirect($url = '/')
    {
        header("Location: {$url}");
        exit;
    }

    /**
     * @param $user
     * @return bool
     */
    public static function checkErrorLogin($user)
    {
        if ((int) $user->login_error >= 3) {
            return true;
        }

        return false;
    }
}