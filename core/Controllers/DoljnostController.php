<?php

namespace Book\Controllers;

use Book\Models\Doljnost;

class DoljnostController extends BaseController
{

    public $pagetitle = 'Управление должностями';

    public $data = [];

    public $action;

    public $deleted;

    public function index()
    {
        $this->data = Doljnost::all();
        $this->deleted = $this->getDeleted();
        echo $this->template->render('Doljnosti/index', $this->getDataToTemplate());
    }

    public function create()
    {
        $this->pagetitle = 'Добавление должности';
        $this->action = 'add';
        echo $this->template->render('Doljnosti/create', $this->getDataToTemplate());
    }

    public function edit($id)
    {
        $this->pagetitle = 'Редактирование должности';
        $this->action = 'update';
        $res = Doljnost::where('id', $id)->get();
        $this->data = $res[0];
        echo $this->template->render('Doljnosti/edit', $this->getDataToTemplate('doljnost'));
    }

    public function delete($id)
    {
        $doljnost = Doljnost::find($id);
        $deleted = $doljnost->delete();
        self::Redirect('/doljnosti/');
    }

    public function add()
    {
        $doljnost = Doljnost::create($this->req->getPost());
        self::Redirect('/doljnosti/');
    }

    public function update()
    {
        $this->data = $this->req->getPost();
        $doljnost = Doljnost::find($this->data['id']);
        foreach($this->data as $key => $value) {
            $doljnost->{$key} = $value;
        }
        $updated = $doljnost->save();
        self::Redirect('/doljnosti/');
    }

    public function getDeleted()
    {
        return Doljnost::onlyTrashed()->get()->toArray();
    }

    public function restore($id = NULL)
    {
        if (empty($id)) {
            Doljnost::onlyTrashed()->restore();
        } else {
            Doljnost::onlyTrashed()->where('id', $id)->restore();
            self::Redirect('/doljnosti/');
        }
    }

}