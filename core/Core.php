<?php

namespace Book;

use Book\DI\DI;
use Book\Modules\Http\Request;
use Book\Modules\Config\Config;
use Book\Modules\Database\Connection;
use Book\Modules\Router\Router;
use Book\Helper\Common;
use Book\Modules\Templates\Template;
use Book\Modules\Thumb\Thumb;
use Book\Modules\Files\Files;

class Core
{
    /**
     * @var DI
     */
    public $di;
    /**
     * @var Request
     */
    public $req;
    /**
     * @var Connection
     */
    public $db;
    /**
     * @var Router
     */
    public $router;
    /**
     * @var Template
     */
    public $template;
    /**
     * @var Config
     */
    public $config;
    /**
     * @var Thumb
     */
    public $thumb;
    /**
     * @var Files
     */
    public $files;


    /**
     * Core constructor.
     */
    public function __construct()
    {
        session_start();

        $this->di = new DI();

        $this->req = new Request();
        $this->di->set('req', $this->req);

        $this->config = new Config($this->di);
        $this->di->set('config', $this->config);
        $this->config->getMainConfig();

        $this->thumb = new Thumb();
        $this->di->set('thumb', $this->thumb);

        $this->files = new Files($this->di);
        $this->di->set('files', $this->files);

        $this->db = new Connection($this->di);
        $this->di->set('db', $this->db);

        $this->router = new Router($this->di);
        $this->di->set('router', $this->router);

        $this->template = new Template();
        $this->di->set('template', $this->template);

    }

    /**
     *
     */
    public function init()
    {
        $this->getRoutes();
        $routerDispatch = $this->router->dispatch($this->req->server['REQUEST_METHOD'], Common::getPatchUrl($this->req));

        if ($routerDispatch == null) {
            $this->router->errorRedirect('404', 404);
        }

        list($class, $action) = explode(':', $routerDispatch->getController(), 2);

        $controller = '\Book\Controllers\\' . $class;
        if (!class_exists($controller)) {
            $controller = '\Book\Controllers\Error\\' . $class;
        }
        $parameters = $routerDispatch->getParameters();

        call_user_func_array([new $controller($this->di), $action], $parameters);
    }

    /**
     *
     */
    public function getRoutes()
    {
        require_once $this->req->server['DOCUMENT_ROOT'] . '/core/Modules/Router/Route.php';
    }
}