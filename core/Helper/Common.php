<?php

namespace Book\Helper;

class Common
{

    public static function getPatchUrl($req) {
        $pathUrl = $req->server['REQUEST_URI'];

        if($position = strpos($pathUrl, '?')) {
            $pathUrl = substr($pathUrl, 0, $position);
        }

        return $pathUrl;
    }
}