<?php

namespace Book\Models;


class WorkShop extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'workshop';
    /**
     * @var array
     */
    protected $fillable = [];
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('\Book\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('\Book\Models\Status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm()
    {
        return $this->belongsTo('\Book\Models\Firm');
    }
}