<?php

namespace Book\Models;

class Visov extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'visovi';
    /**
     * @var array
     */
    protected $fillable = [
        'partner', 'dtfeng', 'dateforengineer', 'timeforengineer', 'home', 'street', 'apartment', 'adress', 'fone', 'namek', 'problemsk', 'engineer', 'status_id', 'housing', 'vkgroup', 'kvitancia', 'sumuslugi', 'detali', 'money', 'idcert', 'sumsale', 'sale', 'koplate', 'schet', 'problemse', 'firm',
    ];
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('\Book\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('\Book\Models\Status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm()
    {
        return $this->belongsTo('\Book\Models\Firm');
    }
}