<?php

namespace Book\Models;

class Status extends BaseModel
{
    protected $table = 'statusi';

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];
}