<?php

namespace Book\Models;

class Doljnost extends BaseModel
{
    protected $table = 'doljnosti';
    protected $fillable = ['doljnost', 'stavka'];
    protected $dates = ['deleted_at'];
}