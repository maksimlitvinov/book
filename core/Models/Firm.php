<?php

namespace Book\Models;

class Firm extends BaseModel
{
    protected $table = 'firms';

    protected $fillable = ['name','fone','email','uindex','uadress','fadress','inn','kpp','oktmo','bank','rs','ks','bik','fdir','idir','odir','osnov','dogovor'];

    protected $dates = ['deleted_at'];
}