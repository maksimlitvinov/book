<?php

namespace Book\Models;

class User extends BaseModel
{
    protected $table = 'users';
    protected $fillable = [
        'login','user_name','last_name','otchestvo','phone','email','doljnost_id','pass','partner_id','salt','images',
    ];
    protected $hidden = ['pass','pass'];
    protected $dates = ['deleted_at'];

    public function doljnost()
    {
        return $this->belongsTo('\Book\Models\Doljnost');
    }
}